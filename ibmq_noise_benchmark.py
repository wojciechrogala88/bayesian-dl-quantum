import numpy as np
import os
from qiskit import execute
from HHL_IBMQ import get_hhl_2x2, create_plot

noise_types = ["Gate Noise", "Measurement Noise"]
noise_levels = [np.linspace(0., 1.0, num=5), np.linspace(0., 1.0, num=5)]
hhl = get_hhl_2x2()

if __name__ == '__main__':
    if not os.path.exists("output"):
        os.mkdir("output")
    if not os.path.exists("figures"):
        os.mkdir("figures")
    for noise_type in range(2):
        for noise_level in noise_levels[noise_type]:
            print("\nSimulating " + noise_types[noise_type], noise_level)
            if noise_type == 0:
                config = {
                    'noise_params': {
                        'U': {
                            'p_pauli': [noise_level, 0, 0]
                        },
                        'readout_error': [0, 0]
                    }
                }
            else:
                config = {
                    'noise_params': {
                        'U': {
                            'p_pauli': [0, 0, 0]
                        },
                        'readout_error': [noise_level, noise_level]
                    }
                }
            results = execute(
                hhl, shots=8192, config=config, backend='local_qasm_simulator')
            count_dict = results.result().get_counts()
            with open('output/IBMQX_' + noise_types[noise_type] \
                          + '_' + str(noise_level) + '_Counts', 'w') as file:
                file.write(str(count_dict))
    create_plot()
